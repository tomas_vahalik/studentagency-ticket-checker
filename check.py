#!/usr/bin/env python
import argparse
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from threading import Thread
import os
from pyvirtualdisplay import Display
import signal
#Keys selenium.webdriver import Firefox # pip install selenium

running = True

def signal_handler(signal, frame):
    print "Quiting.."
    running = False

signal.signal(signal.SIGINT, signal_handler)


class MyApp:
    """Base Class for MyApp"""
    def __init__(self, url, wt, wht):
        self.url = url
        self.waitTime = wt
        self.whatTime = wht

    def run(self):
        display = Display(visible=0, size=(800,600))
        display.start()
        driver = webdriver.Firefox()
        driver.get(self.url)
        
        while running:
            # start = time.time()
            print "Searching..."
            time.sleep(1)
            out = driver.find_elements_by_class_name("free")

            found = False
            
            for item in out:
                # print item.text
                if self.whatTime in item.text:
                    print ":) There is free place! " + str(time.asctime()) + "\n"
                    found = True
                
            if found == False:
                print ":( Not found"
            else:
                os.system('/usr/bin/notify-send -t 6000 "I\'ve found your time, hurry up!" 2>/dev/null')
                os.system('/opt/google/chrome/google-chrome "' + self.url + '"')
                os.system('play /data/projects/webcheck/ALARM.WAV')
                break


            # end = time.time()
            # elapsed = end - start
            # print "Elapsed time:" + str(elapsed)
            print "Going to sleep.."

            driver.refresh()
            time.sleep(self.waitTime)
            

        driver.close()
        display.stop()
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Check if desired time is available')
    parser.add_argument('-t','--time',default='9:05')
    parser.add_argument('-r','--refresh',type=float,default=90.0)
    parser.add_argument('-u','--url',default="http://jizdenky.studentagency.cz/Booking/from/NOVY_JICIN/to/BRNO/tarif/REGULAR/departure/20130218/retdep/20130214/return/false/ropen/false/credit/false/class/2#search-results")

    args = parser.parse_args()

    print "Looking for: " + args.time
    print "Interval: " + str(args.refresh) + "s"
    print "On: " + args.url
    searcher = MyApp(args.url,args.refresh,args.time)

    try:
        t = Thread(target=searcher.run)
        t.start()
    except Exception, e:
        print "some fails :D"
        
