import re
import time

__author__ = 'straiki'

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

running = True


class Checker:
    """Base Class"""

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.counter = 0
        self.resultLine = []
        self.iterations = 10
        self.whatTime = ["16:30", "18:30"]
        self.whatDate = "24.10.14"
        self.driver.get("https://jizdenky.studentagency.cz/")
        self.pattern = re.compile('[0-9]{0,2}:[0-9]{2}')
        # self.driver.implicitly_wait(5)

        self.login()


    def login(self):
        # ##########################################################################

        loginUN = self.driver.find_element_by_id("login_credit")
        loginPASS = self.driver.find_element_by_id("pwd_credit")

        loginUN.send_keys("#PUT_TICKET_NUMBER_HERE#")
        loginPASS.send_keys("#PUT_PASS_HERE#")

        loginPASS.send_keys(Keys.ENTER)

        try:
            infoPanel = WebDriverWait(self.driver, 10).until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, "feedbackPanelINFO"))
            )
        except:
            print("not logged in :( ")
            self.driver.close()
            raise

            # assert "Byli jste úspěšně přihlášeni." in infoPanel[0].text

            # ##########################################################################

    def run(self):
        # ##########################################################################

        global running
        odjezd = self.driver.find_element_by_id("id2e")
        odjezd.send_keys(Keys.SHIFT + Keys.HOME)
        odjezd.send_keys(Keys.DELETE)
        odjezd.send_keys(self.whatDate)
        odjezd.send_keys(Keys.ENTER)

        # ##########################################################################


        while running:
            self.counter += 1
            time.sleep(2)  # wait for ajax
            out = self.driver.find_elements_by_class_name("free")

            found = False

            for item in out:
                # print item.text
                listOut = self.pattern.findall(item.text)
                results = [s for s in listOut if s in self.whatTime]
                if len(results) > 0:
                    print(":) There is free place! " + str(time.asctime()) + "\n")
                    found = True
                    running = False
                    self.resultLine = item

            if not found:
                print(":( Not found " + str(time.asctime()) + " := " + str(self.counter))
                time.sleep(25)
                self.driver.refresh()

            if self.counter > self.iterations:
                running = False
                print("Restarting - new login")
                raise ReferenceError("Restarting..")

        # Buy ticket################################################################

        if found:
            print("Buying ticket")

            p = re.compile('<div id="([A-Za-z0-9]{2,4})" class="col_price"')
            resultID = p.findall(self.resultLine.get_attribute("innerHTML"))

            ticketLink = self.driver.find_element_by_id(resultID[0])
            ticketLink.click()

            time.sleep(5)
            # Listek je vlozen!

            # winsound.PlaySound('Ring08.wav', winsound.SND_FILENAME) #notify?


            continueButton = self.driver.find_element_by_css_selector("button.button")
            continueButton.click()

            time.sleep(10)

            finishButton = self.driver.find_elements_by_css_selector("button.button")[1]
            finishButton.click()

            raise EOFError("Ticket bought, ending..")

    def finish(self, closeWindow: bool):
        """
        :param closeWindow:
        :type closeWindow: bool
        """
        if closeWindow:
            self.driver.close()


if __name__ == "__main__":

    total = 0
    run = True
    while run:
        searcher = Checker()
        time.sleep(5)  # Time to change destination
        try:
            searcher.run()
        except ReferenceError as e:
            print(e)
            searcher.finish(True)
        except EOFError as e:
            print(e)
            searcher.finish(False)
            run = False
        except Exception as e:
            print("Error " + str(time.asctime()) + "\n")
            searcher.finish(False)
        finally:
            total += 1
            print("Refreshed " + str(searcher.counter) + " times!")
            print("Totally " + str(searcher.counter * total) + " times!")
            print("Date: " + str(searcher.whatDate) + ", time:" + str(searcher.whatTime))
            running = True

